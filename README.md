
# Design

`ClndLogger` is an extensible logging framework. The basic concept is that when logging a message, a `LogEvent` is created.
The `LogEvent` is then passed through `Enrichers` to acquire ambient information and finally is emitted to `Sinks` that will
handle the persistence side.

The framework provides a base implementation of `ILogger` with the `Core.Logger` class. The base implementation uses a message processor
to interpolate the message value using the provided parameters. Furthermore, `Core.Logger` can act as a sink for derived loggers in order to 
ensure that only one base logger is ever created. Derived loggers can enrich events by adding properties, emit back to the base logger and
can be disposed at any time without affecting the `Sink`. Only the base logger is responsible to dispose the disposable sinks.

Since a sink can be used concurrently, it is the responsibility of the sink to guarantee synchronization. In the provided
`Sinks.ConsoleSink` a root sync object is provided in order to ensure that any logger emitting events to the console is synchronized.

A very common issue in loggers is providing ambient information throughout the execution of a process. The framework provides
a `LoggingContext` where the user can freely add properties that can be added to the logEvents. The context takes advantage of
the enriching capabilities of the framework to push those properties into the LogEvent when the event is emitted. The context is
implemented using an AsyncLocal ImmutableStack of enrichers. 

# Usage
## Basic usage

```c#
Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Is(LogLevel.Debug)
    .Write.ToConsole()
    .Enrich.WithLoggingContext()
    .CreateLogger();
    
var logger = Log.Logger.ForContext<T>();

var otherLogger = logger.ForContext<T2>();

var correlationId = Guid.NewGuid();
using(LoggingContext.Push(new PropertyEnricher("CorrelationId", correlationId))) 
{
   logger.Warning($"This should have a property CorrelationId with value {correaltionId}");
   otherLogger.Warning($"This should have a property CorrelationId with value {correaltionId}");
}
```


# Sample

A sample console application can be found in `Samples/CldnLogger.ConsoleApp`. A sample json formatter is implemented in `SimpleJsonFormatter`.

# Limitations

Parallel base logger instances share the same LoggingContext. While I believe that it is not a common usage scenario, an alternative solution would be to create a new instance of the logging context per base logger.
In this scenario the base logger should expose an API e.g. `IDisposable BeginScope(params ILogEventEnricher[] enrichers)` which would replace the usage of the
static `LoggingContext` class.

# Planning

Additional effort can be applied in the following areas:
- Documentation!
- Provide a library for integration with Microsoft.Extensions.Logging
- Improve the message template tokenizer to support formatting of the variables
  - Estimated effort: ~2d depending on the feature set we need to support
- Apply the message template tokenizer to the log message in order to support named formats e.g.
  `logger.Information("The value of the request is {Value}", value)` which will allow for better structured logging
  - Estimated effort: 1d
- Implement a json formatter.
  - For a dummy formatter we could even use Newtonsoft.Json to serialize the LogEvent
  - For something more elaborated, we would need to implement our own.
- Implement a file sink that supports rolling and maximum sizes
  - Estimated effort: 1d for something simple
- Improve test coverage to reduce the probability of unexpected bugs

