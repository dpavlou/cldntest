﻿using System.Linq;
using CldnLogger.Enrichers;
using CldnLogger.Enrichers.Context;
using CldnLogger.Tests.Helpers;
using CldnLogger.Tests.Helpers.Sinks;
using Xunit;

namespace CldnLogger.Tests.Enrichers.Context
{
    public class LoggingContextTests
    {
        [Fact]
        public void PushedValuesAreEnrichingLogEvents()
        {
            var sink = new ListSink();

            var logger = new LoggerConfiguration()
                .WriteTo.Sink(sink)
                .Enrich.WithLoggingContext()
                .CreateLogger();

            using (LoggingContext.Push(new PropertyEnricher("X", 1), new PropertyEnricher("Y", 3)))
            {
                logger.Information("test");
                Assert.Single(sink.Events);
                var @event = sink.Events.First();
                Assert.Equal(1, @event.Properties["X"].Extract<int>());
                Assert.Equal(3, @event.Properties["Y"].Extract<int>());
            }
        }

        [Fact]
        public void UsesTheValueFromTheCorrectNestingContext()
        {
            var sink = new ListSink();

            var logger = new LoggerConfiguration()
                .WriteTo.Sink(sink)
                .Enrich.WithLoggingContext()
                .CreateLogger();

            using (LoggingContext.Push(new PropertyEnricher("X", 1), new PropertyEnricher("Y", 3)))
            {
                logger.Information("test");
                var @event = sink.Events.Last();
                Assert.Equal(1, @event.Properties["X"].Extract<int>());
                Assert.Equal(3, @event.Properties["Y"].Extract<int>());

                using (LoggingContext.Push(new PropertyEnricher("X", 4), new PropertyEnricher("Z", 2)))
                {
                    logger.Information("test");
                    @event = sink.Events.Last();
                    Assert.Equal(4, @event.Properties["X"].Extract<int>());
                    Assert.Equal(3, @event.Properties["Y"].Extract<int>());
                    Assert.Equal(2, @event.Properties["Z"].Extract<int>());
                }
                
                logger.Information("test");
                @event = sink.Events.Last();
                Assert.Equal(1, @event.Properties["X"].Extract<int>());
                Assert.Equal(3, @event.Properties["Y"].Extract<int>());
            }
        }
    }
}