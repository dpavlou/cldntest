﻿using CldnLogger.Core;
using CldnLogger.Events;

namespace CldnLogger.Tests.Helpers.Enrichers
{
    public class FixedValueEnricher : ILogEventEnricher
    {
        public FixedValueEnricher()
        {
            
        }
        
        public void Enrich(LogEvent logEvent, IEventPropertyFactory propertyFactory)
        {
            logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty("FixedValue", "AValue"));
        }
    }
}