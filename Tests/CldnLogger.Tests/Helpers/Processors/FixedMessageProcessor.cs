﻿using System;
using CldnLogger.Core;
using CldnLogger.Events;

namespace CldnLogger.Tests.Helpers.Processors
{
    public class FixedMessageProcessor : IMessageProcessor
    {
        private readonly string _fixedMessage;

        public FixedMessageProcessor(string fixedMessage)
        {
            _fixedMessage = fixedMessage ?? throw new ArgumentNullException();
        }
        
        public string Process(string messageTemplate, object[] messageParameters, out EventProperty[] properties)
        {
            properties = Array.Empty<EventProperty>();
            return _fixedMessage;
        }
        
        public EventProperty CreateProperty(string name, object value)
        {
            return new EventProperty(name, new ObjectPropertyValue(value));
        }
    }

    public static class ProcessorConfigurationExtensions
    {
        public static LoggerConfiguration FixedMessage(this LoggerMessageProcessorConfiguration cfg, string message)
        {
            return cfg.Processor(new FixedMessageProcessor(message));
        }
    }
}