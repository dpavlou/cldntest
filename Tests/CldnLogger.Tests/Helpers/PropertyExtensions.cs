﻿using CldnLogger.Core;
using CldnLogger.Events;

namespace CldnLogger.Tests.Helpers
{
    public static class PropertyExtensions
    {
        public static T Extract<T>(this EventPropertyValue pv)
        {
            return (T)((ObjectPropertyValue)pv).Value;
        }
    }
}