﻿using System.Collections.Generic;
using System.IO;
using CldnLogger.Core;
using CldnLogger.Events;
using CldnLogger.Formats;

namespace CldnLogger.Tests.Helpers.Sinks
{
    public class ListSink : ILogEventSink
    {
        public List<LogEvent> Events { get; } = new();
        
        public void Emit(LogEvent logEvent)
        {
            Events.Add(logEvent);
        }
    }

    public class FormattedSink : ILogEventSink
    {
        private readonly IMessageFormatter _formatter;
        public List<string> Events { get; } = new();

        public FormattedSink(IMessageFormatter formatter)
        {
            _formatter = formatter;
        }

        public void Emit(LogEvent logEvent)
        {
            var sw = new StringWriter();
            _formatter.Format(logEvent, sw);
            Events.Add(sw.ToString());
        }
    }
}