﻿using System;
using CldnLogger.Core;
using CldnLogger.Events;

namespace CldnLogger.Tests.Helpers.Sinks
{
    public class DisposableSink : ILogEventSink, IDisposable
    {
        public bool IsDisposed { get; private set; }

        public void Emit(LogEvent logEvent)
        {
        }

        public void Dispose()
        {
            IsDisposed = true;
        }
    }
}