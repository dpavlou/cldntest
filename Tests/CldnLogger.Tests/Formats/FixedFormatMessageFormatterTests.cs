﻿using System.Linq;
using CldnLogger.Formats;
using CldnLogger.Tests.Helpers.Sinks;
using Xunit;

namespace CldnLogger.Tests.Formats
{
    public class FixedFormatMessageFormatterTests
    {
        [Fact]
        public void CanUseFormatter()
        {
            var sink = new FormattedSink(new FixedFormatMessageFormatter());
            var logger = new LoggerConfiguration()
                .WriteTo.Sink(sink)
                .CreateLogger()
                .ForContext<FixedFormatMessageFormatterTests>();

            var msg = "qqqq";
            logger.Information(msg);

            Assert.Single(sink.Events);
            Assert.Contains(nameof(FixedFormatMessageFormatter), sink.Events.First());
            Assert.Contains(msg, sink.Events.First());
        }
        
    }
}