using System;
using System.Linq;
using CldnLogger.Events;
using CldnLogger.Tests.Helpers;
using CldnLogger.Tests.Helpers.Processors;
using CldnLogger.Tests.Helpers.Sinks;
using Xunit;

namespace CldnLogger.Tests
{
    public class LoggerConfigurationTests
    {
        [Fact]
        public void LastMinimumLevelIsKept()
        {
            var sink = new ListSink();
            var logger = new LoggerConfiguration()
                .WriteTo.Sink(sink)
                .MinimumLevel.Is(LogLevel.Debug)
                .MinimumLevel.Is(LogLevel.Warning)
                .CreateLogger();
            
            logger.Trace("test");
            logger.Warning("test");
            logger.Debug("test");

            Assert.Single(sink.Events);
        }

        [Fact]
        public void CanWriteToMultipleSinks()
        {
            var sink = new ListSink();
            var sink2 = new ListSink();
            var logger = new LoggerConfiguration()
                .WriteTo.Sink(sink)
                .WriteTo.Sink(sink2)
                .MinimumLevel.Is(LogLevel.Debug)
                .CreateLogger();
            
            logger.Debug("test");

            Assert.Single(sink.Events);
            Assert.Single(sink2.Events);
        }

        [Fact]
        public void DoesNotBreakWithoutSinks()
        {
            var logger = new LoggerConfiguration()
                .MinimumLevel.Is(LogLevel.Debug)
                .CreateLogger();
            
            logger.Debug("test");
        }

        [Fact]
        public void OnlyTheLastMessageProcessorIsKept()
        {
            var sink = new ListSink();
            var expected = "fixedmessage";
            var logger = new LoggerConfiguration()
                .MinimumLevel.Is(LogLevel.Debug)
                .ProcessUsing.Default()
                .ProcessUsing.FixedMessage(expected)
                .WriteTo.Sink(sink)
                .CreateLogger();
            
            logger.Debug("123123");

            Assert.Single(sink.Events);
            Assert.Collection(sink.Events, x => Assert.Equal(expected, x.Message));
        }

        [Fact]
        public void MultipleEnrichersCanBeApplied()
        {
            var sink = new ListSink();
            var logger = new LoggerConfiguration()
                .MinimumLevel.Is(LogLevel.Debug)
                .WriteTo.Sink(sink)
                .Enrich.WithProperty("first", "first")
                .Enrich.WithProperty("second", 2)
                .CreateLogger();
            
            logger.Debug("test");
            logger.Warning("test");
            Assert.Collection(sink.Events,  
                x =>
                {
                    Assert.Equal(2, x.Properties.Count);
                    Assert.Contains("first", x.Properties);
                    Assert.Contains("second", x.Properties);
                },
                x =>
                {
                    Assert.Equal(2, x.Properties.Count);
                    Assert.Contains("first", x.Properties);
                    Assert.Contains("second", x.Properties);
                });
        }

        [Fact]
        public void DerivedPropertyLoggerAddsProperty()
        {
            var sink = new ListSink();
            var logger = new LoggerConfiguration()
                .WriteTo.Sink(sink)
                .CreateLogger()
                .ForContext("first", 1);
            
            logger.Information("test");

            Assert.Single(sink.Events);
            var latest = sink.Events.Last();
            Assert.Equal(1, latest.Properties["first"].Extract<int>());

        }
        
        [Fact]
        public void CreateLoggerCanBeCalledOnlyOnce()
        {
            var lc = new LoggerConfiguration();
            lc.CreateLogger();
            Assert.Throws<InvalidOperationException>(() => lc.CreateLogger());
        }

        [Fact]
        public void SinksAreDisposedWhenRootLoggerIsDisposed()
        {
            var sink = new DisposableSink();
            var logger = new LoggerConfiguration().WriteTo.Sink(sink).CreateLogger();
            logger.Dispose();
            Assert.True(sink.IsDisposed);
        }

        [Fact]
        public void SinksAreNotDisposedWhenDerivedLoggerIsDisposed()
        {
            var sink = new DisposableSink();
            var logger = new LoggerConfiguration().WriteTo.Sink(sink).CreateLogger()
                .ForContext<LoggerConfigurationTests>();

            ((IDisposable)logger).Dispose();
            Assert.False(sink.IsDisposed);
        }
    }
}