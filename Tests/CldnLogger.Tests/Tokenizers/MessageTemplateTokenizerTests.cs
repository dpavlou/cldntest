﻿using System;
using System.Linq;
using CldnLogger.Tokenizers;
using Xunit;

namespace CldnLogger.Tests.Tokenizers
{
    public class MessageTemplateTokenizerTests
    {
        [Fact]
        public void ThrowsWithNullTemplate()
        {
            var sut = new MessageTemplateTokenizer();
            Assert.Throws<ArgumentNullException>(() => sut.Tokenize(null));
        }
        
        [Fact]
        public void ReturnsASingleTokenWhenNoVariablesPresent()
        {
            var sut = new MessageTemplateTokenizer();
            var input = "12345 asd acvxcvs sdfsd";
            var template = sut.Tokenize(input);

            Assert.Single(template.Tokens);
            Assert.Equal(input, template.Text);
        }

        [Fact]
        public void ReturnsASingleTokenWhenMessageIsEmpty()
        {
            var sut = new MessageTemplateTokenizer();
            var input = "";
            var template = sut.Tokenize(input);

            Assert.Single(template.Tokens);
            Assert.Equal(input, template.Text);
        }

        [Fact]
        public void CorrectlyEscapesCurlyBraces()
        {
            var sut = new MessageTemplateTokenizer();
            var input = "one two {{three four}}";
           
            var template = sut.Tokenize(input);

            Assert.Single(template.Tokens);
            Assert.Equal("one two {three four}", template.Text);
        }
        
        [Fact]
        public void UsesTwoTextTokensIfInvalidStartOfVariableAtTheEnd()
        {
            var sut = new MessageTemplateTokenizer();
            var input = "12323 {";

            var template = sut.Tokenize(input);

            Assert.Equal(2, template.Tokens.Count());
            Assert.Equal(input, template.Text);
            Assert.Collection(template.Tokens,
                x => Assert.IsType<TextToken>(x),
                x => Assert.IsType<TextToken>(x));
        }

        [Fact]
        public void UsesTextTokenWhenInvalidStartOfVariable()
        {
            var sut = new MessageTemplateTokenizer();
            var input = "a {b z";

            var template = sut.Tokenize(input);

            Assert.Equal(3, template.Tokens.Count());
            Assert.Equal(input, template.Text);
            Assert.Collection(template.Tokens,
                x =>
                {
                    Assert.IsType<TextToken>(x);
                    Assert.Equal("a ", ((TextToken)x).Value);
                },
                x =>
                {
                    Assert.IsType<TextToken>(x);
                    Assert.Equal("{b", ((TextToken)x).Value);
                },
                x =>
                {
                    Assert.IsType<TextToken>(x);
                    Assert.Equal(" z", ((TextToken)x).Value);
                });
        }

        [Fact]
        public void UsesPropertyTokenWhenValidVariable()
        {
            var sut = new MessageTemplateTokenizer();
            var input = "{b}";
            
            var template = sut.Tokenize(input);

            Assert.Single(template.Tokens);
            var token = template.Tokens.First();
            Assert.IsType<PropertyToken>(token);
            var pt = (PropertyToken)token;
            Assert.Equal("b", pt.PropertyName);
            Assert.Equal("{b}", pt.ToString());
        }

        [Fact]
        public void UsesTextTokenForEmptyVariable()
        {
            var sut = new MessageTemplateTokenizer();
            var input = "{}";
            
            var template = sut.Tokenize(input);

            Assert.Single(template.Tokens);
            var token = template.Tokens.First();
            Assert.IsType<TextToken>(token);
            var tt = (TextToken)token;
            Assert.Equal("{}", tt.ToString());
        }
        
    }
}