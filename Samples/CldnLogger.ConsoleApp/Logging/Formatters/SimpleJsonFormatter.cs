﻿using System.IO;
using CldnLogger.Events;
using CldnLogger.Formats;
using Newtonsoft.Json;

namespace CldnLogger.ConsoleApp.Logging.Formatters
{
    public class SimpleJsonFormatter : IMessageFormatter
    {
        private readonly JsonSerializerSettings _settings;
        
        public SimpleJsonFormatter(bool pretty = false)
        {
            _settings = new JsonSerializerSettings()
            {
                Formatting = pretty ? Formatting.Indented : Formatting.None,
            };
        }
        
        public void Format(LogEvent logEvent, TextWriter output)
        {
            var json = JsonConvert.SerializeObject(logEvent, _settings);
            output.Write(json);
        }
    }
}