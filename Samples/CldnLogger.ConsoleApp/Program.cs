﻿using System;
using CldnLogger.ConsoleApp.Logging.Formatters;
using CldnLogger.Core;
using CldnLogger.Enrichers;
using CldnLogger.Enrichers.Context;
using CldnLogger.Events;
using CldnLogger.Formats;
using CldnLogger.Sinks;

namespace CldnLogger.ConsoleApp
{
    class Program
    {
        static void Main()
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Is(LogLevel.Debug)
                .Enrich.WithLoggingContext()
                // .WriteTo.Console()
                .WriteTo.Console(new PropertyMessageFormatter($"[{{{BuiltInProperties.Timestamp}}} {{{BuiltInProperties.Level}}} {{{BuiltInProperties.SourceContext}}} {{CorrelationId}}] {{{BuiltInProperties.Message}}} {{{BuiltInProperties.Exception}}} {{{BuiltInProperties.Properties}}}\n"))
                // .WriteTo.Console(new SimpleJsonFormatter(true))
                .WriteTo.File("test-formatted.log", new PropertyMessageFormatter($"[{{{BuiltInProperties.Timestamp}}} {{{BuiltInProperties.Level}}} {{{BuiltInProperties.SourceContext}}}] {{{BuiltInProperties.Message}}} {{{BuiltInProperties.Exception}}} {{{BuiltInProperties.Properties}}}\n"))
                .WriteTo.File("test.log.json", new SimpleJsonFormatter(true))
                .CreateLogger();

            var independentLogger =
                new LoggerConfiguration().WriteTo.Console().Enrich.WithLoggingContext().CreateLogger()
                    .ForContext(typeof(Logger));

            var mainLogger = Log.ForContext<Program>();
            
            mainLogger.Trace("trace {0}", 0);
            mainLogger.Debug("debug {0}", "rerr");
            mainLogger.Information("information {0}", 2);
            mainLogger.Warning("warning {0}", 3);
            mainLogger.Error("error {0}", 4);

            var exception = new ArgumentNullException("test");
            mainLogger.Error(exception, "Error with exception");


            var cLogger = mainLogger.ForContext(Constants.SourceContextPropertyName, "test1");

            var correlationId = Guid.NewGuid();
            using(LoggingContext.Push(new PropertyEnricher("CorrelationId", correlationId)))
            {
                cLogger.Information("Value first {0}", correlationId);

                var newCorrelationId = Guid.NewGuid();
                using (LoggingContext.Push(new PropertyEnricher("CorrelationId", newCorrelationId)))
                {
                    independentLogger.Warning("Testing props");
                    cLogger.Information("Value second {0}", newCorrelationId);
                }
                
                cLogger.Information("Value third {0}", correlationId);
            }
            
            mainLogger.Information("--- done");
            Log.CloseAndFlush();
            
        }
    }
}