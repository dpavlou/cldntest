﻿using CldnLogger.Events;

namespace CldnLogger.Core
{
    public interface IMessageProcessor : IEventPropertyFactory
    {
        string Process(string messageTemplate, object[] messageParameters, out EventProperty[] properties);
    }
}