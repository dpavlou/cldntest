using CldnLogger.Events;

namespace CldnLogger.Core
{
    public interface ILogEventEnricher
    {
        void Enrich(LogEvent logEvent, IEventPropertyFactory propertyFactory);
    }
}