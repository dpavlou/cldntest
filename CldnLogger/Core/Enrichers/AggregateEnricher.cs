﻿using System;
using System.Collections.Generic;
using System.Linq;
using CldnLogger.Events;

namespace CldnLogger.Core.Enrichers
{
    internal class AggregateEnricher : ILogEventEnricher
    {
        private readonly ILogEventEnricher[] _enrichers;

        public AggregateEnricher(IEnumerable<ILogEventEnricher> enrichers)
        {
            _enrichers = enrichers?.ToArray() ?? throw new ArgumentNullException(nameof(enrichers));
        }
        
        public void Enrich(LogEvent logEvent, IEventPropertyFactory propertyFactory)
        {
            foreach (var enricher in _enrichers)
            {
                try
                {
                    enricher.Enrich(logEvent, propertyFactory);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: {0}", ex.ToString());
                }
            }
        }
    }
}