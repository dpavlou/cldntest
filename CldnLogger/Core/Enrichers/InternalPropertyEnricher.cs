using System;
using CldnLogger.Events;

namespace CldnLogger.Core.Enrichers
{
    internal class InternalPropertyEnricher : ILogEventEnricher
    {
        private readonly EventProperty _eventProperty;

        public InternalPropertyEnricher(EventProperty eventProperty)
        {
            _eventProperty = eventProperty ?? throw new ArgumentNullException(nameof(eventProperty));
        }
        
        public void Enrich(LogEvent logEvent, IEventPropertyFactory propertyFactory)
        {
            if (logEvent is null) throw new ArgumentNullException(nameof(logEvent));

            logEvent.AddPropertyIfAbsent(_eventProperty);
        }
    }
}