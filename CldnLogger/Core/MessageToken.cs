﻿using System.IO;
using CldnLogger.Events;

namespace CldnLogger.Core
{
    public abstract class MessageToken
    {
        protected MessageToken(int startIndex)
        {
            StartIndex = startIndex;
        }
        
        public int StartIndex { get; }
        public abstract int Length { get; }

        public abstract void Render(string propertyName, EventPropertyValue value, TextWriter output);

        public abstract override string ToString();
    }
}