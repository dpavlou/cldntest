﻿using System;
using CldnLogger.Events;

namespace CldnLogger.Core
{
    internal sealed class NoopLogger : ILogger
    {
        public static readonly ILogger Instance = new NoopLogger();

        public ILogger ForContext<T>()
        {
            return this;
        }

        public ILogger ForContext(Type source)
        {
            return this;
        }

        public ILogger ForContext(string propertyName, object value)
        {
            return this;
        }

        public bool IsEnabled(LogLevel level) => false;

        private NoopLogger()
        {
        }
        
        public void Write(LogEvent logEvent)
        {
        }

        public void Write(LogLevel level, Exception exception, string message, params object[] args)
        {
        }

        public void Trace(Exception exception, string message, params object[] args)
        {
        }

        public void Trace(string message, params object[] args)
        {
        }

        public void Debug(Exception exception, string message, params object[] args)
        {
        }

        public void Debug(string message, params object[] args)
        {
        }

        public void Information(Exception exception, string message, params object[] args)
        {
        }

        public void Information(string message, params object[] args)
        {
        }

        public void Warning(Exception exception, string message, params object[] args)
        {
        }

        public void Warning(string message, params object[] args)
        {
        }

        public void Error(Exception exception, string message, params object[] args)
        {
        }

        public void Error(string message, params object[] args)
        {
        }

        public void Fatal(Exception exception, string message, params object[] args)
        {
        }

        public void Fatal(string message, params object[] args)
        {
        }
    }
}