﻿using System.Linq;
using CldnLogger.Events;

namespace CldnLogger.Core
{
    public class DefaultMessageProcessor : IMessageProcessor
    {

        public string Process(string messageTemplate, object[] messageParameters, out EventProperty[] properties)
        {   
            var text = string.Format(messageTemplate, messageParameters);

            properties = messageParameters?.Select((x, index) => CreateProperty($"Param{index}", x)).ToArray();
            
            return text;
        }
        
        public EventProperty CreateProperty(string name, object value)
        {
            return new EventProperty(name, new ObjectPropertyValue(value));
        }

    }
}