﻿using System;
using System.Globalization;
using System.IO;
using CldnLogger.Events;

namespace CldnLogger.Core
{
    public class ObjectPropertyValue : EventPropertyValue
    {
        public object Value { get; }

        public ObjectPropertyValue(object value)
        {
            Value = value;
        }

        public override void Render(TextWriter output)
        {
            if (Value is IFormattable formattable)
            {
                output.Write(formattable.ToString(null, CultureInfo.InvariantCulture));
            }
            else
            {
                output.Write(Value.ToString());
            }
        }
    }
}