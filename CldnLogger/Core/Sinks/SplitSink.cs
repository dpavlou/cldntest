using System;
using CldnLogger.Events;

namespace CldnLogger.Core.Sinks
{
    internal class SplitSink : ILogEventSink, IDisposable
    {
        private readonly ILogger _logger;
        private readonly bool _attemptDispose;

        public SplitSink(ILogger logger, bool attemptDispose = false)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _attemptDispose = attemptDispose;
        }
        
        public void Emit(LogEvent logEvent)
        {
            if (logEvent is null) throw new ArgumentNullException(nameof(logEvent));
            var copy = logEvent.Copy();
            _logger.Write(copy);
        }

        public void Dispose()
        {
            if (!_attemptDispose)
            {
                return;
            }
            (_logger as IDisposable)?.Dispose();
        }
    }
}