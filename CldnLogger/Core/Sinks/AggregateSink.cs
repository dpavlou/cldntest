﻿using System;
using System.Collections.Generic;
using System.Linq;
using CldnLogger.Events;

namespace CldnLogger.Core.Sinks
{
    internal class AggregateSink : ILogEventSink
    {
        private readonly ILogEventSink[] _sinks;

        public AggregateSink(IEnumerable<ILogEventSink> sinks)
        {
            _sinks = sinks?.ToArray() ?? throw new ArgumentNullException(nameof(sinks));
        }

        public void Emit(LogEvent logEvent)
        {
            foreach (var sink in _sinks)
            {
                try
                {
                    sink.Emit(logEvent);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: {0}", ex.ToString());
                }
            }
        }
    }
}