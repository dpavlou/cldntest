﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CldnLogger.Core
{
    public class MessageTemplate
    {
        private readonly MessageToken[] _tokens;
        
        
        public MessageTemplate(IEnumerable<MessageToken> tokens)
            : this(string.Concat(tokens), tokens)
        {
        }

        public MessageTemplate(string text, IEnumerable<MessageToken> tokens)
        {
            Text = text ?? throw new ArgumentNullException(nameof(text));
            _tokens = (tokens ?? throw new ArgumentNullException(nameof(tokens))).ToArray();
        }
        
        public string Text { get; }

        public IEnumerable<MessageToken> Tokens => _tokens;
        
        public override string ToString() => Text;
    }
}