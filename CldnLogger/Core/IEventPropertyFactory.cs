﻿using CldnLogger.Events;

namespace CldnLogger.Core
{
    public interface IEventPropertyFactory
    {
        EventProperty CreateProperty(string name, object value);
    }
}