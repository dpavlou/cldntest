﻿using CldnLogger.Events;

namespace CldnLogger.Core
{
    public interface ILogEventSink
    {
        void Emit(LogEvent logEvent);
    }
}