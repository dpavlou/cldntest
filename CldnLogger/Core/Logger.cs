﻿using System;
using CldnLogger.Core.Enrichers;
using CldnLogger.Events;

namespace CldnLogger.Core
{
    public sealed class Logger : ILogger, ILogEventSink, IDisposable
    {
        private readonly IMessageProcessor _messageProcessor;
        private readonly ILogEventSink _sink;
        private readonly Action _dispose;
        private readonly ILogEventEnricher _enricher;
        private readonly LogLevel _minimumLevel;
        
        public Logger(IMessageProcessor messageProcessor, LogLevel minimumLevel, ILogEventSink sink, ILogEventEnricher enricher, Action dispose = null)
        {
            _messageProcessor = messageProcessor;
            _sink = sink;
            _dispose = dispose;
            _enricher = enricher;
            _minimumLevel = minimumLevel;
        }

        public ILogger ForContext(string propertyName, object value)
        {
            if (string.IsNullOrWhiteSpace(propertyName))
            {
                return this;
            }

            var enricher = new InternalPropertyEnricher(new EventProperty(propertyName, new ObjectPropertyValue(value)));

            return new Logger(_messageProcessor, _minimumLevel, this, enricher, null);
        }

        public ILogger ForContext(Type source)
        {
            if (source == null!)
                return this; 

            return ForContext(Constants.SourceContextPropertyName, source.FullName);
        }

        public ILogger ForContext<TSource>() => ForContext(typeof(TSource));

        internal ILogger ForContext(ILogEventEnricher enricher)
        {
            if (enricher is null) return this;
            return new Logger(_messageProcessor, _minimumLevel, this, enricher, null);
        }
        
        public bool IsEnabled(LogLevel level)
        {
            return (int)level >= (int)_minimumLevel;
        }

        public void Write(LogEvent logEvent)
        {
            if (logEvent is null) return;
            if (!IsEnabled(logEvent.Level)) return;
            Dispatch(logEvent);
        }

        private void Dispatch(LogEvent logEvent)
        {
            try
            {
                _enricher.Enrich(logEvent, _messageProcessor);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            _sink.Emit(logEvent);
        }

        public void Dispose()
        {
            _dispose?.Invoke();
        }

        public void Emit(LogEvent logEvent)
        {
            if (logEvent is null) throw new ArgumentNullException(nameof(logEvent));
            Dispatch(logEvent);
        }

        public void Write(LogLevel level, Exception exception, string message,
            params object[] args)
        {
            if (!IsEnabled(level)) return;
            if (message is null) return;

            var text = _messageProcessor.Process(message, args, out var properties);

            Write(new LogEvent(DateTimeOffset.Now, level, exception, text, properties));
        }

        public void Trace(Exception exception, string message, params object[] args)
            => Write(LogLevel.Trace, exception, message, args);

        public void Trace(string message, params object[] args)
            => Write(LogLevel.Trace, null, message, args);

        public void Debug(Exception exception, string message, params object[] args)
            => Write(LogLevel.Debug, exception, message, args);

        public void Debug(string message, params object[] args)
            => Write(LogLevel.Debug, null, message, args);

        public void Information(Exception exception, string message, params object[] args)
            => Write(LogLevel.Information, exception, message, args);

        public void Information(string message, params object[] args)
            => Write(LogLevel.Information, null, message, args);

        public void Warning(Exception exception, string message, params object[] args)
            => Write(LogLevel.Warning, exception, message, args);

        public void Warning(string message, params object[] args)
            => Write(LogLevel.Warning, null, message, args);

        public void Error(Exception exception, string message, params object[] args)
            => Write(LogLevel.Error, exception, message, args);

        public void Error(string message, params object[] args)
            => Write(LogLevel.Error, null, message, args);

        public void Fatal(Exception exception, string message, params object[] args)
            => Write(LogLevel.Fatal, exception, message, args);

        public void Fatal(string message, params object[] args)
            => Write(LogLevel.Fatal, null, message, args);
    }
}