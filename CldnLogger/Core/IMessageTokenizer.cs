﻿namespace CldnLogger.Core
{
    public interface IMessageTokenizer
    {
        MessageTemplate Tokenize(string messageTemplate);
    }
}