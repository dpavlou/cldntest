using System;
using CldnLogger.Core;
using CldnLogger.Core.Sinks;

namespace CldnLogger
{
    public class LoggerSinkConfiguration
    {
        private readonly LoggerConfiguration _loggerConfiguration;
        private readonly Action<ILogEventSink> _addSink;

        public LoggerSinkConfiguration(LoggerConfiguration loggerConfiguration, Action<ILogEventSink> addSink)
        {
            _loggerConfiguration = loggerConfiguration ?? throw new ArgumentNullException(nameof(loggerConfiguration));
            _addSink = addSink ?? throw new ArgumentNullException(nameof(addSink));
        }
        
        public LoggerConfiguration Sink(ILogEventSink sink)
        {
            if (sink is null) throw new ArgumentNullException(nameof(sink));

            _addSink(sink);
            return _loggerConfiguration;
        }
        
        internal LoggerConfiguration Logger(ILogger logger)
        {
            if (logger is null) throw new ArgumentNullException(nameof(logger));

            var secondarySink = new SplitSink(logger);
            return Sink(secondarySink);
        }
    }
}