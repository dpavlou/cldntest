using System;
using CldnLogger.Core;

namespace CldnLogger
{
    public class LoggerMessageProcessorConfiguration
    {
        private readonly LoggerConfiguration _loggerConfiguration;
        private readonly Action<IMessageProcessor> _setProcessor;

        public LoggerMessageProcessorConfiguration(LoggerConfiguration loggerConfiguration, Action<IMessageProcessor> setProcessor)
        {
            _loggerConfiguration = loggerConfiguration;
            _setProcessor = setProcessor;
        }

        public LoggerConfiguration Default()
        {
            _setProcessor(new DefaultMessageProcessor());
            return _loggerConfiguration;
        }

        public LoggerConfiguration Processor(IMessageProcessor processor)
        {
            _setProcessor(processor ?? throw new ArgumentNullException(nameof(processor)));
            return _loggerConfiguration;
        }
    }
}