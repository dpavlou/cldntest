using System;
using CldnLogger.Events;

namespace CldnLogger
{
    public class LoggerMinimumLevelConfiguration
    {
        private readonly LoggerConfiguration _loggerConfiguration;
        private readonly Action<LogLevel> _setMinimum;

        public LoggerMinimumLevelConfiguration(LoggerConfiguration loggerConfiguration, Action<LogLevel> setMinimum)
        {
            _loggerConfiguration = loggerConfiguration ?? throw new ArgumentNullException(nameof(loggerConfiguration));
            _setMinimum = setMinimum ?? throw new ArgumentNullException(nameof(setMinimum));
        }

        public LoggerConfiguration Is(LogLevel minimumLevel)
        {
            _setMinimum(minimumLevel);
            return _loggerConfiguration;
        }
    }
}