﻿using System;
using CldnLogger.Core;
using CldnLogger.Enrichers;
using CldnLogger.Enrichers.Context;

namespace CldnLogger
{
    public class LoggerEnrichmentConfiguration
    {
        private readonly LoggerConfiguration _loggerConfiguration;
        private readonly Action<ILogEventEnricher> _addEnricher;

        internal LoggerEnrichmentConfiguration(
            LoggerConfiguration loggerConfiguration,
            Action<ILogEventEnricher> addEnricher)
        {
            _loggerConfiguration = loggerConfiguration ?? throw new ArgumentNullException(nameof(loggerConfiguration));
            _addEnricher = addEnricher ?? throw new ArgumentNullException(nameof(addEnricher));
        }

        public LoggerConfiguration With(params ILogEventEnricher[] enrichers)
        {
            if (enrichers == null) throw new ArgumentNullException(nameof(enrichers));

            foreach (var logEventEnricher in enrichers)
            {
                if (logEventEnricher is null) throw new ArgumentException("Null enricher is not allowed");
                _addEnricher(logEventEnricher);
            }

            return _loggerConfiguration;
        }

        public LoggerConfiguration WithLoggingContext()
        {
            _addEnricher(new LoggingContextEnricher());

            return _loggerConfiguration;
        }
        
        public LoggerConfiguration With<TEnricher>()
            where TEnricher : ILogEventEnricher, new()
        {
            return With(new TEnricher());
        }
        
        public LoggerConfiguration WithProperty(string name, object value)
        {
                return With(new PropertyEnricher(name, value));
        }
    }
}