﻿using System;
using System.IO;
using CldnLogger.Core;
using CldnLogger.Events;

namespace CldnLogger.Tokenizers
{
    public sealed class TextToken : MessageToken
    {
        public TextToken(string text, int startIndex) : base(startIndex)
        {
            Value = text;
        }
        
        public string Value { get; }
        public override int Length => Value.Length;
        
        public override void Render(string propertyName, EventPropertyValue value, TextWriter output)
        {
            if (output is null) throw new ArgumentNullException(nameof(output));
            output.Write(Value);
        }

        public override string ToString() => Value;
    }
}