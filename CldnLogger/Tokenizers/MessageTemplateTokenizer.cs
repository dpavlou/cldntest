﻿using System;
using System.Collections.Generic;
using System.Text;
using CldnLogger.Core;

namespace CldnLogger.Tokenizers
{
    public class MessageTemplateTokenizer : IMessageTokenizer
    {
        public MessageTemplate Tokenize(string messageTemplate)
        {
            if (messageTemplate is null) throw new ArgumentNullException(nameof(messageTemplate));

            var tokens = MessageTemplateTokenizer.TokenizeInternal(messageTemplate);

            return new MessageTemplate(tokens);
        }

        private static IEnumerable<MessageToken> TokenizeInternal(string mt)
        {
            var tokens = new List<MessageToken>();
            if (mt.Length == 0)
            {
                tokens.Add(new TextToken("", 0));
                return tokens;
            }

            var currentIndex = 0;
            while (true)
            {
                var startText = currentIndex;
                
                var sb = new StringBuilder();
                do
                {
                    var nextChar = mt[currentIndex];
                    // check template variable start
                    if (nextChar == '{')
                    {
                        // two { ({{) is escaped {
                        if (currentIndex + 1 < mt.Length && mt[currentIndex + 1] == '{')
                        {
                            sb.Append(nextChar);
                            currentIndex++;
                        }
                        else
                        {
                            // this is the start of a variable
                            break;
                        }
                    }
                    else
                    {
                        sb.Append(nextChar);
                        // check for variable end
                        if (nextChar == '}')
                        {
                            // check escaped and skip since it was added
                            if (currentIndex + 1 < mt.Length && mt[currentIndex + 1] == '}')
                            {
                                currentIndex++;
                            }
                        }
                    }

                    currentIndex++;
                } while (currentIndex < mt.Length);
                
                if (startText < currentIndex)
                {
                    tokens.Add(new TextToken(sb.ToString(), startText));
                }

                if (currentIndex == mt.Length)
                {
                    break;
                }

                var startProperty = currentIndex;

                // extract PropertyName
                // skip {
                currentIndex++;
                while (currentIndex < mt.Length && IsValidCharacterInPropertyName(mt[currentIndex]))
                {
                    currentIndex++;
                }
                
                // at the end of string or found invalid character
                if (currentIndex == mt.Length || mt[currentIndex] != '}')
                {
                    tokens.Add(new TextToken(mt.Substring(startProperty, currentIndex - startProperty), startProperty));
                }
                else
                {
                    // skip '}'
                    currentIndex++;

                    var rawPropName = mt.Substring(startProperty, currentIndex - startProperty);
                    var variableName = rawPropName.Substring(1, rawPropName.Length - 2);

                    if (variableName.Length == 0)
                    {
                        tokens.Add(new TextToken(rawPropName, startProperty));
                    }
                    else
                    {
                        tokens.Add(new PropertyToken(rawPropName, startProperty, variableName));
                    }
                }
                
                if (currentIndex == mt.Length)
                {
                    break;
                }
            }

            return tokens;
        }

        public static bool IsValidCharacterInPropertyName(char c)
        {
            return char.IsLetterOrDigit(c);
        }
    }
}