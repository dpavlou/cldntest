﻿using System;
using System.IO;
using CldnLogger.Core;
using CldnLogger.Events;

namespace CldnLogger.Tokenizers
{
    public sealed class PropertyToken : MessageToken
    {
        private readonly string _rawText;

        public PropertyToken(string rawText, int startIndex, string propertyName) : base(startIndex)
        {
            if (string.IsNullOrEmpty(propertyName)) throw new ArgumentNullException(nameof(propertyName));

            _rawText = rawText;
            PropertyName = propertyName;
        }

        public string PropertyName { get; }
        public override string ToString() => _rawText;
        public override int Length => _rawText.Length;
        
        public override void Render(string propertyName, EventPropertyValue value, TextWriter output)
        {
            throw new NotImplementedException();
        }
    }
}