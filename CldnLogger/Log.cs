﻿using System;
using System.Threading;
using CldnLogger.Core;

namespace CldnLogger
{
    public static class Log
    {
        private static ILogger _logger = NoopLogger.Instance;

        public static ILogger Logger
        {
            get => _logger;
            set => _logger = value ?? throw new ArgumentNullException(nameof(value));
        }

        public static ILogger ForContext<T>() => Logger.ForContext<T>();

        public static ILogger ForContext(Type source) => Logger.ForContext(source);
        
        public static void CloseAndFlush()
        {
            var logger = Interlocked.Exchange(ref _logger, NoopLogger.Instance);
            
            (logger as IDisposable)?.Dispose();
        }
        
        
    }
}