﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using CldnLogger.Core;
using CldnLogger.Events;

namespace CldnLogger.Formats
{
    public class FixedFormatMessageFormatter : IMessageFormatter
    {
        private readonly IFormatProvider _formatProvider;

        public FixedFormatMessageFormatter()
        {
            _formatProvider = CultureInfo.InvariantCulture;
        }
        
        public void Format(LogEvent logEvent, TextWriter output)
        {
            var sb = new StringBuilder(300);

            sb.Append('[');
            sb.Append(logEvent.Timestamp.UtcDateTime.ToString(_formatProvider));
            sb.Append(' ');
            sb.Append(logEvent.Level);
            
            
            if (logEvent.Properties.TryGetValue(Constants.SourceContextPropertyName, out var sourceContext))
            {
                sb.Append(' ');
                var sw = new StringWriter(sb);
                sourceContext.Render(sw);
            }
            
            sb.Append("] ");

            sb.Append(logEvent.Message);

            if (logEvent.Exception is not null)
            {
                sb.Append(" - Exception: ");
                sb.Append(logEvent.Exception);
            }

            var restOfProperties = logEvent.Properties.Keys.Where(x => x != Constants.SourceContextPropertyName).ToArray();

            if (restOfProperties.Length > 0)
            {
                sb.Append(" - Properties:");
            }
            
            foreach (var key in restOfProperties)
            {
                sb.Append('(');
                var prop = logEvent.Properties[key];
                sb.Append(key);
                sb.Append(": ");
                
                var sw = new StringWriter(sb);
                prop.Render(sw);
                sb.Append(')');
            }

            output.WriteLine(sb.ToString());
        }
    }
}