﻿using System.IO;
using CldnLogger.Events;

namespace CldnLogger.Formats
{
    public interface IMessageFormatter
    {
        void Format(LogEvent logEvent, TextWriter output);
    }
}