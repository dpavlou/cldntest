﻿namespace CldnLogger.Formats
{
    public class BuiltInProperties
    {
        public const string Timestamp = "Timestamp";
        public const string Level = "Level";
        public const string SourceContext = "SourceContext";
        public const string Message = "Message";
        public const string Exception = "Exception";
        public const string Properties = "Properties";
    }
}