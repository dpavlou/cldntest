﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using CldnLogger.Core;
using CldnLogger.Events;
using CldnLogger.Tokenizers;

namespace CldnLogger.Formats
{
    public class PropertyMessageFormatter : IMessageFormatter
    {
        private readonly string _messageTemplate;
        private readonly MessageTemplate _tokenizedTemplate;
        private readonly IFormatProvider _formatProvider;

        public PropertyMessageFormatter(string messageTemplate)
        {
            _messageTemplate = messageTemplate ?? throw new ArgumentNullException(nameof(messageTemplate));
            _tokenizedTemplate = new MessageTemplateTokenizer().Tokenize(messageTemplate);
            _formatProvider = CultureInfo.InvariantCulture;
        }
        
        public void Format(LogEvent logEvent, TextWriter output)
        {
            foreach (var token in _tokenizedTemplate.Tokens)
            {
                if (token is TextToken tt)
                {
                    tt.Render(null, null, output);
                }

                if (token is PropertyToken pt)
                {
                    switch (pt.PropertyName)
                    {
                        case BuiltInProperties.Timestamp:
                            output.Write(logEvent.Timestamp.UtcDateTime.ToString(_formatProvider));
                            break;
                        case BuiltInProperties.Exception:
                            if (logEvent.Exception is not null)
                            {
                                output.Write(logEvent.Exception.ToString());
                            }
                            break;
                        case BuiltInProperties.Level:
                            output.Write(logEvent.Level.ToString());
                            break;
                        case BuiltInProperties.Message:
                            output.Write(logEvent.Message);
                            break;
                        case BuiltInProperties.SourceContext:
                            RenderProperty(logEvent, Constants.SourceContextPropertyName, output);
                            break;
                        case BuiltInProperties.Properties:
                            var keys = logEvent.Properties.Keys.ToArray();

                            if (keys.Length > 0)
                            {
                                output.Write(" | ");
                            }
                            
                            var allExceptLast = keys.Length - 1;
                            
                            for (int i = 0; i < allExceptLast; i++)
                            {
                                var key = keys[i];
                                var prop = logEvent.Properties[key];
                                output.Write($"{key}: ");
                                RenderProperty(keys[i], prop, output);
                                output.Write(" | ");
                            }
                            

                            if (keys.Length > 0)
                            {
                                var last = keys[^1];
                                output.Write($"{last}: ");
                                var prop = logEvent.Properties[last];
                                RenderProperty(last, prop, output);
                            }
                   
                            break;
                        default:
                            RenderProperty(logEvent, pt.PropertyName, output);
                            break;
                    }
                }
                
            }
        }

        private static void RenderProperty(LogEvent logEvent, string propertyName, TextWriter output)
        {
            if (logEvent.Properties.TryGetValue(propertyName, out var propertyValue))
            {
                RenderProperty(propertyName, propertyValue, output);
            }
        }

        private static void RenderProperty(string propertyName, EventPropertyValue value, TextWriter output)
        {
            value.Render(output);
        }
    }
}