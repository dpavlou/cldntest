﻿using System;
using System.IO;
using CldnLogger.Core;
using CldnLogger.Events;
using CldnLogger.Formats;

namespace CldnLogger.Sinks
{
    public sealed class FileSink : ILogEventSink, IDisposable
    {
        private readonly TextWriter _output;
        private readonly IMessageFormatter _formatter;
        private readonly FileStream _stream;
        readonly object _syncRoot = new object();
        
        internal FileSink(string path, IMessageFormatter formatter)
        {
            if (string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException(path);
            _formatter = formatter ?? throw new ArgumentNullException(nameof(formatter));

            CreateDirectoryIfMissing(path);

            _stream = File.Open(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read);
            _stream.Seek(0, SeekOrigin.End);

            _output = new StreamWriter(_stream);
        }
        
        public void Emit(LogEvent logEvent)
        {
            if (logEvent is null) throw new ArgumentNullException(nameof(logEvent));
            lock (_syncRoot)
            {
                _formatter.Format(logEvent, _output);
                _output.Flush();
            }
        }

        public void Dispose()
        {
            lock (_syncRoot)
            {
                _output.Dispose();
            }
        }

        static void CreateDirectoryIfMissing(string path)
        {
            var directory = Path.GetDirectoryName(path);
            if (!string.IsNullOrWhiteSpace(directory) && !Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }
    }

    public static class FileSinkConfigurationExtensions
    {
        public static LoggerConfiguration File(this LoggerSinkConfiguration cfg, string path, IMessageFormatter messageFormatter = null)
        {
            return cfg.Sink(new FileSink(path, messageFormatter ?? new FixedFormatMessageFormatter()));
        }
    }
}