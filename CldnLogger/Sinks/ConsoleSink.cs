﻿using System;
using CldnLogger.Core;
using CldnLogger.Events;
using CldnLogger.Formats;

namespace CldnLogger.Sinks
{
    public class ConsoleSink : ILogEventSink
    {
        private readonly IMessageFormatter _messageFormatter;
        private readonly object _syncRoot;

        public ConsoleSink(IMessageFormatter messageFormatter, object syncRoot)
        {
            _messageFormatter = messageFormatter;
            _syncRoot = syncRoot ?? throw new ArgumentNullException(nameof(syncRoot));
        }

        public void Emit(LogEvent logEvent)
        {
            var output = (logEvent.Level >= LogLevel.Error ? Console.Error : Console.Out);

            lock (_syncRoot)
            {
                _messageFormatter.Format(logEvent, output);
            }
        }
    }

    public static class ConsoleSinkConfigurationExtensions
    {
        static readonly object DefaultSyncRoot = new object();

        public static LoggerConfiguration Console(this LoggerSinkConfiguration cfg,
            IMessageFormatter messageFormatter = null)
        {
            return cfg.Sink(new ConsoleSink(messageFormatter ?? new FixedFormatMessageFormatter(),
                DefaultSyncRoot));
        }
    }
}