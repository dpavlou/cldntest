﻿using System;
using System.Collections.Generic;

namespace CldnLogger.Events
{
    public class LogEvent
    {
        private readonly Dictionary<string, EventPropertyValue> _properties;
        public DateTimeOffset Timestamp { get; }
        public LogLevel Level { get; }
        public Exception Exception { get; }
        public string Message { get; }
        public IReadOnlyDictionary<string, EventPropertyValue> Properties => _properties;

        public LogEvent(DateTimeOffset timestamp, LogLevel level, Exception exception, string message, Dictionary<string, EventPropertyValue> properties)
        {
            _properties = properties ?? new Dictionary<string, EventPropertyValue>();
            Timestamp = timestamp;
            Level = level;
            Exception = exception;

            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentNullException(nameof(message));
            }
            
            Message = message;
        }

        internal LogEvent(DateTimeOffset timestamp, LogLevel level, Exception exception, string message,
            EventProperty[] properties)
            : this(timestamp, level, exception, message, new Dictionary<string, EventPropertyValue>())
        {
            if (properties is not null)
            {
                foreach (var t in properties)
                {
                    _properties[t.Name] = t.Value;
                }
            }
        }

        public override string ToString() => Message;

        public LogEvent Copy()
        {
            var properties = new Dictionary<string, EventPropertyValue>(Properties.Count);
            foreach (var key in _properties.Keys)
            {
                properties.Add(key, _properties[key]);
            }

            return new LogEvent(Timestamp, Level, Exception, Message, properties);

        }

        public void AddPropertyIfAbsent(EventProperty eventProperty)
        {
            if (eventProperty is null) throw new ArgumentNullException(nameof(eventProperty));

            if (!_properties.ContainsKey(eventProperty.Name))
            {
                _properties.Add(eventProperty.Name, eventProperty.Value);
            }
        }
    }
}