using System;
using System.IO;

namespace CldnLogger.Events
{
    public abstract class EventPropertyValue : IFormattable
    {
        public abstract void Render(TextWriter output);
        
        public override string ToString() => ToString(null, null);

        public string ToString(string format, IFormatProvider formatProvider)
        {
            var output = new StringWriter();
            Render(output);
            return output.ToString();
        }
    }
}