using System;

namespace CldnLogger.Events
{
    public class EventProperty
    {
        public string Name { get; }
        public EventPropertyValue Value { get; }

        public EventProperty(string name, EventPropertyValue value)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException(nameof(name));
            }
            Name = name;
            Value = value ?? throw new ArgumentNullException(nameof(value));
        }

        public override bool Equals(object obj)
        {
            return obj is EventProperty other && Equals(other);
        }
        
        public bool Equals(EventProperty other)
        {
            return string.Equals(Name, other.Name) && Equals(Value, other.Value);
        }

        public override int GetHashCode()
        {
            return (Name.GetHashCode() * 397) ^ (Value.GetHashCode());
        }
    }
}