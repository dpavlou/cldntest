﻿using System;
using System.Collections.Generic;
using System.Linq;
using CldnLogger.Core;
using CldnLogger.Core.Enrichers;
using CldnLogger.Core.Sinks;
using CldnLogger.Events;

namespace CldnLogger
{
    public class LoggerConfiguration
    {
        private readonly List<ILogEventSink> _sinks = new();
        private readonly List<ILogEventEnricher> _enrichers = new();
        private bool _loggerCreated = false;
        private LogLevel _minimumLevel = LogLevel.Information;
        private IMessageProcessor _messageProcessor = new DefaultMessageProcessor();

        public LoggerConfiguration()
        {
            WriteTo = new LoggerSinkConfiguration(this, s => _sinks.Add(s));
            Enrich = new LoggerEnrichmentConfiguration(this, s => _enrichers.Add(s));
            ProcessUsing = new LoggerMessageProcessorConfiguration(this, s => _messageProcessor = s);
            MinimumLevel = new LoggerMinimumLevelConfiguration(this, s => _minimumLevel = s);
        }
        
        public LoggerSinkConfiguration WriteTo { get; }
        public LoggerEnrichmentConfiguration Enrich { get; }

        public LoggerMessageProcessorConfiguration ProcessUsing { get; }
        
        public LoggerMinimumLevelConfiguration MinimumLevel { get; }
        
        public Logger CreateLogger()
        {
            if (_loggerCreated) throw new InvalidOperationException("was previously called");

            _loggerCreated = true;

            ILogEventSink sink = new AggregateSink(_sinks);

            ILogEventEnricher enricher = new AggregateEnricher(_enrichers);

            var disposableSinks = _sinks.OfType<IDisposable>().ToArray();
            
            void Dispose()
            {
                foreach (var disposable in disposableSinks)
                {
                    disposable.Dispose();
                }
            }

            return new Logger(_messageProcessor, _minimumLevel, sink, enricher, Dispose);
        }
    }
}