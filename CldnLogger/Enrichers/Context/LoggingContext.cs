﻿using System;
using System.Collections.Immutable;
using System.Threading;
using CldnLogger.Core;
using CldnLogger.Events;

namespace CldnLogger.Enrichers.Context
{
    public static class LoggingContext
    {
        private static readonly AsyncLocal<ImmutableStack<ILogEventEnricher>> Data = new();

        private static ImmutableStack<ILogEventEnricher> Enrichers
        {
            get => Data.Value ?? ImmutableStack<ILogEventEnricher>.Empty;
            set => Data.Value = value.IsEmpty ? null : value;
        }

        public static IDisposable Push(params ILogEventEnricher[] enrichers)
        {
            if (enrichers is null) throw new ArgumentNullException(nameof(enrichers));

            var stack = Enrichers;
            var checkpoint = new Checkpoint(stack);

            for (var i = 0; i < enrichers.Length; i++)
            {
                stack = stack.Push(enrichers[i]);
            }

            Enrichers = stack;

            return checkpoint;
        }

        internal static void Enrich(LogEvent logEvent, IEventPropertyFactory propertyFactory)
        {
            var enrichers = Enrichers;
            if (enrichers is null || enrichers == ImmutableStack<ILogEventEnricher>.Empty)
                return;

            foreach (var enricher in enrichers)
            {
                enricher.Enrich(logEvent, propertyFactory);
            }
        }

        private class Checkpoint : IDisposable
        {
            private readonly ImmutableStack<ILogEventEnricher> _data;

            public Checkpoint(ImmutableStack<ILogEventEnricher> data)
            {
                _data = data;
            }


            public void Dispose()
            {
                Enrichers = _data;
            }
        }
        
    }
}