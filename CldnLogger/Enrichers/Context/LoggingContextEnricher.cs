﻿using CldnLogger.Core;
using CldnLogger.Events;

namespace CldnLogger.Enrichers.Context
{
    public class LoggingContextEnricher : ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, IEventPropertyFactory propertyFactory)
        {
            LoggingContext.Enrich(logEvent, propertyFactory);
        }
    }
}