﻿using CldnLogger.Core;
using CldnLogger.Events;

namespace CldnLogger.Enrichers
{
    public class PropertyEnricher : ILogEventEnricher
    {
        private readonly string _name;
        private readonly object _value;

        public PropertyEnricher(string name, object value)
        {
            _name = name;
            _value = value;
        }

        public void Enrich(LogEvent logEvent, IEventPropertyFactory propertyFactory)
        {
            var property = propertyFactory.CreateProperty(_name, _value);
            logEvent.AddPropertyIfAbsent(property);
        }
    }
}