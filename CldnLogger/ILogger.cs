﻿using System;
using CldnLogger.Events;

namespace CldnLogger
{
    public interface ILogger
    {
        ILogger ForContext<T>();

        ILogger ForContext(Type source);

        ILogger ForContext(string propertyName, object value);
        
        bool IsEnabled(LogLevel level) => true;

        void Write(LogLevel level, Exception exception, string message, params object[] args);

        void Write(LogEvent logEvent);

        void Trace(Exception exception, string message, params object[] args);

        void Trace(string message, params object[] args);

        void Debug(Exception exception, string message, params object[] args);

        void Debug(string message, params object[] args);

        void Information(Exception exception, string message, params object[] args);

        void Information(string message, params object[] args);

        void Warning(Exception exception, string message, params object[] args);

        void Warning(string message, params object[] args);

        void Error(Exception exception, string message, params object[] args);

        void Error(string message, params object[] args);

        void Fatal(Exception exception, string message, params object[] args);

        void Fatal(string message, params object[] args);

    }
}